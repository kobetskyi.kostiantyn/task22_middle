'use strict';

window.addEventListener('DOMContentLoaded', () => {

    const rowsInput = document.querySelector('#row-input');
    const colsInput = document.querySelector('#col-input');
    const sbmtBtm = document.querySelector('#submit-btn');
    const tableContainer = document.querySelector('#table-container');
    const form = document.querySelector('#form');


    function setDefault() {
        rowsInput.value = 0;
        colsInput.value = 0;
    }
    setDefault();

    const createTable = () => {
        let rows = rowsInput.value;
        let cols = colsInput.value;

        const table = document.createElement('table');

        for (let i = 0; i < cols; i++) {
            const col = document.createElement('col');
            col.setAttribute('width', '100');
            table.appendChild(col);
        }

        for (let i = 0; i < rows; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < cols; j++) {
                const cell = document.createElement('td');
                cell.innerText = `${i+1}${j+1}`;
                cell.classList.add('cell');
                row.appendChild(cell);
            }
            table.appendChild(row);
        }

        tableContainer.appendChild(table);
    };

    const clearTable = () => {
        tableContainer.innerHTML = '';
    };


    sbmtBtm.addEventListener('click', event => {
        event.preventDefault();
        clearTable();
        createTable();
        setDefault();
    });

    function generateRandomColor() {
        let color = Math.floor(Math.random() * 16777215).toString(16);
        return `#${color}`;
    }

    tableContainer.addEventListener('click', (event) => {
        const target = event.target;
        if (target && target.classList.contains('cell') && !target.classList.contains('color')) {
            target.setAttribute('bgcolor', generateRandomColor());
            target.classList.add('color');
        } else if (target && target.classList.contains('cell') && target.classList.contains('color')) {
            target.setAttribute('bgcolor', 'white');
            target.classList.remove('color');
        }
    });
});